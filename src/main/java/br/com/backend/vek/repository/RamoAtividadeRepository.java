package br.com.backend.vek.repository;

import br.com.backend.vek.model.RamoAtividade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RamoAtividadeRepository extends JpaRepository<RamoAtividade, Long> {
}
