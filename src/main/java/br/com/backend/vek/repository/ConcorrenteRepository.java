package br.com.backend.vek.repository;

import br.com.backend.vek.model.Concorrente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConcorrenteRepository extends JpaRepository<Concorrente, Long> {
}
