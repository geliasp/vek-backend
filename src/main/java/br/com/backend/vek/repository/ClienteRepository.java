package br.com.backend.vek.repository;

import br.com.backend.vek.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    Cliente getClienteByCpf(String cpf);
}
