package br.com.backend.vek.rest.impl;

import br.com.backend.vek.facade.ConcorrenteFacade;
import br.com.backend.vek.model.Concorrente;
import br.com.backend.vek.rest.ConcorrenteRestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ConcorrenteRestBeanImpl implements ConcorrenteRestBean {

    @Autowired
    ConcorrenteFacade concorrenteFacade;

    @Override
    public Concorrente create(Concorrente concorrente) {
        return concorrenteFacade.create(concorrente);
    }

    @Override
    public List<Concorrente> read() {
        return concorrenteFacade.read();
    }

    @Override
    public Concorrente update(Concorrente concorrente) {
        return concorrenteFacade.update(concorrente);
    }

    @Override
    public void delete(Long id) {
        concorrenteFacade.delete(id);
    }
}
