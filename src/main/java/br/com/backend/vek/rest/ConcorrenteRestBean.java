package br.com.backend.vek.rest;

import br.com.backend.vek.model.Concorrente;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public interface ConcorrenteRestBean {
    Concorrente create(Concorrente concorrente);

    List<Concorrente> read();

    Concorrente update(Concorrente concorrente);

    void delete(Long id);
}
