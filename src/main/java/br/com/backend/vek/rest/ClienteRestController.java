package br.com.backend.vek.rest;

import br.com.backend.vek.model.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cliente")
@CrossOrigin
public interface ClienteRestController {

    @PostMapping
    ResponseEntity<Cliente> create(@RequestBody Cliente cliente);

    @GetMapping
    ResponseEntity<List<Cliente>> read();

    @PutMapping
    ResponseEntity<Cliente> update(@Valid @RequestBody Cliente cliente);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable Long id);
}
