package br.com.backend.vek.rest.impl;

import br.com.backend.vek.holder.PropostaCSV;
import br.com.backend.vek.model.Proposta;
import br.com.backend.vek.rest.PropostaRestBean;
import br.com.backend.vek.rest.PropostaRestController;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/proposta")
@CrossOrigin
public class PropostaRestControllerImpl implements PropostaRestController {

    @Autowired
    PropostaRestBean propostaRestBean;

    @Override
    public ResponseEntity<Proposta> create(Proposta proposta) {
        Proposta propostaSalvo = propostaRestBean.create(proposta);
        if (ObjectUtils.isNotEmpty(propostaSalvo)) {
            return ResponseEntity.status(HttpStatus.OK).body(propostaSalvo);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @Override
    public ResponseEntity<List<Proposta>> read() {
        List<Proposta> propostas = propostaRestBean.read();
        if (!propostas.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(propostas);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @Override
    public ResponseEntity<Proposta> update(@Valid Proposta proposta) {
        try {
            Proposta propostaSalvo = propostaRestBean.update(proposta);
            return ResponseEntity.status(HttpStatus.OK).body(propostaSalvo);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @Override
    public void delete(Long id) {
        propostaRestBean.delete(id);
    }

    @Override
    public void csv(HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        String nomeArquivo = "propostas.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + nomeArquivo + "\"");

        StatefulBeanToCsv<PropostaCSV> writer = new StatefulBeanToCsvBuilder<PropostaCSV>(
                response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        writer.write(propostaRestBean.csv());
    }
}
