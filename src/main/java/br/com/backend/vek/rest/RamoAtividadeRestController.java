package br.com.backend.vek.rest;

import br.com.backend.vek.model.RamoAtividade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ramoAtividade")
@CrossOrigin
public interface RamoAtividadeRestController {

    @PostMapping
    ResponseEntity<RamoAtividade> create(@RequestBody RamoAtividade ramoAtividade);

    @GetMapping
    ResponseEntity<List<RamoAtividade>> read();

    @PutMapping
    ResponseEntity<RamoAtividade> update(@Valid @RequestBody RamoAtividade ramoAtividade);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable Long id);

}
