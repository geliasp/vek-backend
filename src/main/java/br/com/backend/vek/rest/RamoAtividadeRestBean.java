package br.com.backend.vek.rest;

import br.com.backend.vek.model.RamoAtividade;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public interface RamoAtividadeRestBean {
    RamoAtividade create(RamoAtividade ramoAtividade);

    List<RamoAtividade> read();

    RamoAtividade update(RamoAtividade ramoAtividade);

    void delete(Long id);
}
