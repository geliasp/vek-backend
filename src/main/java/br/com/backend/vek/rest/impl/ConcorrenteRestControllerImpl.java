package br.com.backend.vek.rest.impl;

import br.com.backend.vek.model.Concorrente;
import br.com.backend.vek.rest.ConcorrenteRestBean;
import br.com.backend.vek.rest.ConcorrenteRestController;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/concorrente")
@CrossOrigin
public class ConcorrenteRestControllerImpl implements ConcorrenteRestController {

    @Autowired
    ConcorrenteRestBean concorrenteRestBean;

    @PostMapping
    @Override
    public ResponseEntity<Concorrente> create(@Valid @RequestBody Concorrente concorrente) {
        Concorrente concorrenteSalvo = concorrenteRestBean.create(concorrente);
        if (ObjectUtils.isNotEmpty(concorrenteSalvo)) {
            return ResponseEntity.status(HttpStatus.OK).body(concorrenteSalvo);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping
    @Override
    public ResponseEntity<List<Concorrente>> read() {
        List<Concorrente> concorrentes = concorrenteRestBean.read();
        if (!concorrentes.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(concorrentes);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PutMapping
    @Override
    public ResponseEntity<Concorrente> update(@Valid @RequestBody Concorrente concorrente) {
        try {
            Concorrente concorrenteSalvo = concorrenteRestBean.update(concorrente);
            return ResponseEntity.status(HttpStatus.OK).body(concorrenteSalvo);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public void delete(@PathVariable Long id) {
        concorrenteRestBean.delete(id);
    }
}
