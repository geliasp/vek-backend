package br.com.backend.vek.rest;

import br.com.backend.vek.holder.PropostaCSV;
import br.com.backend.vek.model.Proposta;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public interface PropostaRestBean {
    Proposta create(Proposta proposta);

    List<Proposta> read();

    Proposta update(Proposta proposta);

    void delete(Long id);

    List<PropostaCSV> csv();
}
