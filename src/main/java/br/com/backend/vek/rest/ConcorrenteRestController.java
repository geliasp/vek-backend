package br.com.backend.vek.rest;

import br.com.backend.vek.model.Concorrente;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/concorrente")
@CrossOrigin
public interface ConcorrenteRestController {
    @PostMapping
    ResponseEntity<Concorrente> create(@RequestBody Concorrente concorrente);

    @GetMapping
    ResponseEntity<List<Concorrente>> read();

    @PutMapping
    ResponseEntity<Concorrente> update(@Valid @RequestBody Concorrente concorrente);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable Long id);
}
