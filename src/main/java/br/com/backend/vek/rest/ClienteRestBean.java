package br.com.backend.vek.rest;

import br.com.backend.vek.model.Cliente;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public interface ClienteRestBean {
    Cliente create(Cliente cliente);

    List<Cliente> read();

    Cliente update(Cliente cliente);

    void delete(Long id);
}
