package br.com.backend.vek.rest.impl;

import br.com.backend.vek.facade.ClienteFacade;
import br.com.backend.vek.model.Cliente;
import br.com.backend.vek.rest.ClienteRestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ClienteRestBeanImpl implements ClienteRestBean {

    @Autowired
    ClienteFacade clienteFacade;

    @Override
    public Cliente create(Cliente cliente) {
        return clienteFacade.create(cliente);
    }

    @Override
    public List<Cliente> read() {
        return clienteFacade.read();
    }

    @Override
    public Cliente update(Cliente cliente) {
        return clienteFacade.update(cliente);
    }

    @Override
    public void delete(Long id) {
        clienteFacade.delete(id);
    }
}
