package br.com.backend.vek.rest.impl;

import br.com.backend.vek.facade.RamoAtividadeFacade;
import br.com.backend.vek.model.RamoAtividade;
import br.com.backend.vek.rest.RamoAtividadeRestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class RamoAtividadeRestBeanImpl implements RamoAtividadeRestBean {

    @Autowired
    RamoAtividadeFacade ramoAtividadeFacade;

    @Override
    public RamoAtividade create(RamoAtividade ramoAtividade) {
        return ramoAtividadeFacade.create(ramoAtividade);
    }

    @Override
    public List<RamoAtividade> read() {
        return ramoAtividadeFacade.read();
    }

    @Override
    public RamoAtividade update(RamoAtividade ramoAtividade) {
        return ramoAtividadeFacade.update(ramoAtividade);
    }

    @Override
    public void delete(Long id) {
        ramoAtividadeFacade.delete(id);
    }
}
