package br.com.backend.vek.rest.impl;

import br.com.backend.vek.model.RamoAtividade;
import br.com.backend.vek.rest.RamoAtividadeRestBean;
import br.com.backend.vek.rest.RamoAtividadeRestController;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ramoAtividade")
@CrossOrigin
public class RamoAtividadeRestControllerImpl implements RamoAtividadeRestController {

    @Autowired
    RamoAtividadeRestBean ramoAtividadeRestBean;

    @PostMapping
    @Override
    public ResponseEntity<RamoAtividade> create(@Valid @RequestBody RamoAtividade ramoAtividade) {
        RamoAtividade ramoAtividadeSalvo = ramoAtividadeRestBean.create(ramoAtividade);
        if (ObjectUtils.isNotEmpty(ramoAtividadeSalvo)) {
            return ResponseEntity.status(HttpStatus.OK).body(ramoAtividadeSalvo);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping
    @Override
    public ResponseEntity<List<RamoAtividade>> read() {
        List<RamoAtividade> ramoAtividades = ramoAtividadeRestBean.read();
        if (!ramoAtividades.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(ramoAtividades);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PutMapping
    @Override
    public ResponseEntity<RamoAtividade> update(@Valid @RequestBody RamoAtividade ramoAtividade) {
        try {
            RamoAtividade ramoAtividadeSalvo = ramoAtividadeRestBean.update(ramoAtividade);
            return ResponseEntity.status(HttpStatus.OK).body(ramoAtividadeSalvo);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public void delete(@PathVariable Long id) {
        ramoAtividadeRestBean.delete(id);
    }
}
