package br.com.backend.vek.rest;

import br.com.backend.vek.model.Proposta;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/proposta")
@CrossOrigin
public interface PropostaRestController {

    @PostMapping
    ResponseEntity<Proposta> create(@RequestBody Proposta proposta);

    @GetMapping
    ResponseEntity<List<Proposta>> read();

    @PutMapping
    ResponseEntity<Proposta> update(@Valid @RequestBody Proposta proposta);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable Long id);

    @GetMapping("/csv")
    void csv(HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException;
}
