package br.com.backend.vek.rest.impl;

import br.com.backend.vek.model.Cliente;
import br.com.backend.vek.rest.ClienteRestBean;
import br.com.backend.vek.rest.ClienteRestController;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cliente")
@CrossOrigin
public class ClienteRestControllerImpl implements ClienteRestController {

    @Autowired
    ClienteRestBean clienteRestBean;

    @PostMapping
    @Override
    public ResponseEntity<Cliente> create(@Valid @RequestBody Cliente cliente) {
        Cliente clienteSalvo = clienteRestBean.create(cliente);
        if (ObjectUtils.isNotEmpty(clienteSalvo)) {
            return ResponseEntity.status(HttpStatus.OK).body(clienteSalvo);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping
    @Override
    public ResponseEntity<List<Cliente>> read() {
        List<Cliente> clientes = clienteRestBean.read();
        if (!clientes.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(clientes);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PutMapping
    @Override
    public ResponseEntity<Cliente> update(@Valid @RequestBody Cliente cliente) {
        try {
            Cliente clienteSalvo = clienteRestBean.update(cliente);
            return ResponseEntity.status(HttpStatus.OK).body(clienteSalvo);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Override
    public void delete(@PathVariable Long id) {
        clienteRestBean.delete(id);
    }
}
