package br.com.backend.vek.rest.impl;

import br.com.backend.vek.facade.ClienteFacade;
import br.com.backend.vek.facade.ConcorrenteFacade;
import br.com.backend.vek.facade.PropostaFacade;
import br.com.backend.vek.holder.PropostaCSV;
import br.com.backend.vek.model.Cliente;
import br.com.backend.vek.model.Concorrente;
import br.com.backend.vek.model.Proposta;
import br.com.backend.vek.rest.PropostaRestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PropostaRestBeanImpl implements PropostaRestBean {

    @Autowired
    PropostaFacade propostaFacade;

    @Autowired
    ClienteFacade clienteFacade;

    @Autowired
    ConcorrenteFacade concorrenteFacade;

    @Override
    public Proposta create(Proposta proposta) {
        return propostaFacade.create(proposta);
    }

    @Override
    public List<Proposta> read() {
        return propostaFacade.read();
    }

    @Override
    public Proposta update(Proposta proposta) {
        return propostaFacade.update(proposta);
    }

    @Override
    public void delete(Long id) {
        propostaFacade.delete(id);
    }

    @Override
    public List<PropostaCSV> csv() {
        List<PropostaCSV> propostaCSVs = new ArrayList<PropostaCSV>();

        List<Proposta> propostas = propostaFacade.read();
        List<Cliente> clientes = clienteFacade.read();
        List<Concorrente> concorrentes = concorrenteFacade.read();

        for (Proposta proposta : propostas) {

            if (proposta.isAceita()) {
                Cliente cliente = clientes
                        .stream()
                        .filter(item -> proposta.getFkCliente().equals(item.getId()))
                        .findAny()
                        .orElse(null);
                assert cliente != null;

                Concorrente concorrente = concorrentes
                        .stream()
                        .filter(item -> proposta.getFkConcorrente().equals(item.getId()))
                        .findAny()
                        .orElse(null);
                assert concorrente != null;

                propostaCSVs.add(
                        PropostaCSV.builder()
                                .concorrente(concorrente.getNome())
                                .clienteCPF(cliente.getCpf())
                                .clienteTelefone(cliente.getTelefone())
                                .clienteEmail(cliente.getEmail())
                                .ramoAtividade(cliente.getRamoAtividade().getNome())
                                .creditoConcorrente(proposta.getCreditoConcorrente())
                                .debitoConcorrente(proposta.getDebitoConcorrente())
                                .creditoAceito(proposta.getCreditoProposta())
                                .debitoAceito(proposta.getDebitoProposta())
                                .build()
                );
            }

        }


        return propostaCSVs;
    }


}
