package br.com.backend.vek.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tb_ramo_atividade")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "clientes"})
@EqualsAndHashCode(exclude = "clientes")
@ToString(exclude = "clientes")
public class RamoAtividade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column
    private String nome;

    @NotNull
    @Column(name = "taxa_credito_minimo")
    private Double taxaCreditoMinimo;


    @NotNull
    @Column(name = "taxa_debito_minimo")
    private Double taxaDebitoMinimo;

    @OneToMany(mappedBy = "ramoAtividade")
    private List<Cliente> clientes;
}
