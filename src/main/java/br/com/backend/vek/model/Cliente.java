package br.com.backend.vek.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tb_cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @CPF
    @NotNull
    @Column(name = "cpf")
    private String cpf;

    @NotNull
    @Email
    @Column(name = "email")
    private String email;

    @NotNull
    @Pattern(regexp = "[0-9]{11}")
    private String telefone;

    @ManyToOne
    @JoinColumn(name="fk_ramo_atividade", nullable=false)
    private RamoAtividade ramoAtividade;
}
