package br.com.backend.vek.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tb_proposta")
@EntityListeners(AuditingEntityListener.class)
@EnableJpaAuditing
public class Proposta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "fk_cliente")
    private Long fkCliente;

    @NotNull
    @Column(name = "fk_ramo_atividade")
    private Long fkRamoAtividade;

    @NotNull
    @Column(name = "fk_concorrente")
    private Long fkConcorrente;

    @NotNull
    @Column(name = "credito_concorrente")
    private Double creditoConcorrente;

    @NotNull
    @Column(name = "debito_concorrente")
    private Double debitoConcorrente;

    @NotNull
    @Column(name = "credito_proposta")
    private Double creditoProposta;

    @NotNull
    @Column(name = "debito_proposta")
    private Double debitoProposta;

    @NotNull
    @Column(name = "aceita")
    private boolean aceita;

    @CreationTimestamp
    @Column(name = "data_aceite")
    private Date dataAceite;

}
