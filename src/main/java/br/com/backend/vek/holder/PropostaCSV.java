package br.com.backend.vek.holder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PropostaCSV {
    private String concorrente;
    private String clienteCPF;
    private String clienteTelefone;
    private String clienteEmail;
    private String ramoAtividade;
    private Double creditoConcorrente;
    private Double debitoConcorrente;
    private Double creditoAceito;
    private Double debitoAceito;
    private Date dataDeAceite;
}
