package br.com.backend.vek.facade.impl;

import br.com.backend.vek.facade.PropostaFacade;
import br.com.backend.vek.holder.PropostaCSV;
import br.com.backend.vek.model.Proposta;
import br.com.backend.vek.repository.PropostaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("propostaFacade")
@Transactional(propagation = Propagation.REQUIRED)
public class PropostaFacadeImpl implements PropostaFacade {

    @Autowired
    PropostaRepository propostaRepository;

    @Override
    public Proposta create(Proposta proposta) {
        return propostaRepository.save(proposta);
    }

    @Override
    public List<Proposta> read() {
        return propostaRepository.findAll();
    }

    @Override
    public Proposta update(Proposta proposta) {
        Proposta propostaSalvo = getPropostaById(proposta.getId());
        BeanUtils.copyProperties(proposta, propostaSalvo, "id");
        return propostaRepository.save(propostaSalvo);
    }

    @Override
    public void delete(Long id) {
        propostaRepository.deleteById(id);
    }

    private Proposta getPropostaById(Long id) {
        Proposta propostaSalvo = propostaRepository.getOne(id);
        if (propostaSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return propostaSalvo;
    }

}
