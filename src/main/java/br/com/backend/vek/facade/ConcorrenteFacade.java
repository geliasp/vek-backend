package br.com.backend.vek.facade;

import br.com.backend.vek.model.Concorrente;

import java.util.List;

public interface ConcorrenteFacade {
    Concorrente create(Concorrente concorrente);

    List<Concorrente> read();

    Concorrente update(Concorrente concorrente);

    void delete(Long id);
}
