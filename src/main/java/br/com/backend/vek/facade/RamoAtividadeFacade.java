package br.com.backend.vek.facade;

import br.com.backend.vek.model.RamoAtividade;

import java.util.List;

public interface RamoAtividadeFacade {
    RamoAtividade create(RamoAtividade ramoAtividade);

    List<RamoAtividade> read();

    RamoAtividade update(RamoAtividade ramoAtividade);

    void delete(Long id);
}
