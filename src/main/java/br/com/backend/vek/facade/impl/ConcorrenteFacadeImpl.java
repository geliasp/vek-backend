package br.com.backend.vek.facade.impl;

import br.com.backend.vek.facade.ConcorrenteFacade;
import br.com.backend.vek.model.Concorrente;
import br.com.backend.vek.repository.ConcorrenteRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("concorrenteFacade")
@Transactional(propagation = Propagation.REQUIRED)
public class ConcorrenteFacadeImpl implements ConcorrenteFacade {

    @Autowired
    ConcorrenteRepository concorrenteRepository;

    @Override
    public Concorrente create(Concorrente concorrente) {
        return concorrenteRepository.save(concorrente);
    }

    @Override
    public List<Concorrente> read() {
        return concorrenteRepository.findAll();
    }

    @Override
    public Concorrente update(Concorrente concorrente) {
        Concorrente concorrenteSalvo = getConcorrenteById(concorrente.getId());
        BeanUtils.copyProperties(concorrente, concorrenteSalvo, "id");
        return concorrenteRepository.save(concorrenteSalvo);
    }

    @Override
    public void delete(Long id) {
        concorrenteRepository.deleteById(id);
    }

    private Concorrente getConcorrenteById(Long id) {
        Concorrente concorrenteSalvo = concorrenteRepository.getOne(id);
        if (concorrenteSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return concorrenteSalvo;
    }
}
