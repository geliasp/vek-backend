package br.com.backend.vek.facade.impl;

import br.com.backend.vek.facade.ClienteFacade;
import br.com.backend.vek.model.Cliente;
import br.com.backend.vek.repository.ClienteRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("clienteFacade")
@Transactional(propagation = Propagation.REQUIRED)
public class ClienteFacadeImpl implements ClienteFacade {

    @Autowired
    ClienteRepository clienteRepository;

    @Override
    public Cliente create(Cliente cliente) {
        Cliente clienteExistente = clienteRepository.getClienteByCpf(cliente.getCpf());
        if (clienteExistente == null) {
            return clienteRepository.save(cliente);
        } else {
            return clienteExistente;
        }
    }

    @Override
    public List<Cliente> read() {
        return clienteRepository.findAll();
    }

    @Override
    public Cliente update(Cliente cliente) {
        Cliente clienteSalvo = getClienteById(cliente.getId());
        BeanUtils.copyProperties(cliente, clienteSalvo, "id");
        return clienteRepository.save(clienteSalvo);
    }

    @Override
    public void delete(Long id) {
        clienteRepository.deleteById(id);
    }

    private Cliente getClienteById(Long id) {
        Cliente clienteSalvo = clienteRepository.getOne(id);
        if (clienteSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return clienteSalvo;
    }
}
