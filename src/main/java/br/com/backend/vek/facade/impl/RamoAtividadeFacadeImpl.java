package br.com.backend.vek.facade.impl;

import br.com.backend.vek.facade.RamoAtividadeFacade;
import br.com.backend.vek.model.RamoAtividade;
import br.com.backend.vek.repository.RamoAtividadeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ramoAtividadeFacade")
@Transactional(propagation = Propagation.REQUIRED)
public class RamoAtividadeFacadeImpl implements RamoAtividadeFacade {

    @Autowired
    RamoAtividadeRepository ramoAtividadeRepository;

    @Override
    public RamoAtividade create(RamoAtividade ramoAtividade) {
        return ramoAtividadeRepository.save(ramoAtividade);
    }

    @Override
    public List<RamoAtividade> read() {
        return ramoAtividadeRepository.findAll();
    }

    @Override
    public RamoAtividade update(RamoAtividade ramoAtividade) {
        RamoAtividade ramoAtividadeSalvo = getRamoAtividadeSalvoById(ramoAtividade.getId());
        BeanUtils.copyProperties(ramoAtividade, ramoAtividadeSalvo, "id");
        return ramoAtividadeRepository.save(ramoAtividadeSalvo);
    }

    @Override
    public void delete(Long id) {
        ramoAtividadeRepository.deleteById(id);
    }

    private RamoAtividade getRamoAtividadeSalvoById(Long id) {
        RamoAtividade ramoAtividadeSalvo = ramoAtividadeRepository.getOne(id);
        if (ramoAtividadeSalvo == null) {
            throw new EmptyResultDataAccessException(1);
        }
        return ramoAtividadeSalvo;
    }
}
