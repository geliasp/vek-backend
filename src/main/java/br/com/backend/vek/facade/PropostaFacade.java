package br.com.backend.vek.facade;

import br.com.backend.vek.holder.PropostaCSV;
import br.com.backend.vek.model.Proposta;

import java.util.List;

public interface PropostaFacade {
    Proposta create(Proposta proposta);

    List<Proposta> read();

    Proposta update(Proposta proposta);

    void delete(Long id);

}
