package br.com.backend.vek.facade;

import br.com.backend.vek.model.Cliente;

import java.util.List;

public interface ClienteFacade {
    Cliente create(Cliente cliente);

    List<Cliente> read();

    Cliente update(Cliente cliente);

    void delete(Long id);
}
