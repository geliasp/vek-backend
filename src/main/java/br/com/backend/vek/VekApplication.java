package br.com.backend.vek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VekApplication {

    public static void main(String[] args) {
        SpringApplication.run(VekApplication.class, args);
    }

}
